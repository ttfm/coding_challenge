import Types from '../actions/types';

const _initialState = {
  fxRate: null,
  loading: false,
  error: null
}

const exchangeReducer = (oldState = _initialState, action) => {
  switch(action.type) {
    case Types.START_POLLING_EXCHANGE_RATE:
      return {
        ...oldState,
        loading: true,
        error: null,
        fxRate: null
      }

    case Types.STOP_POLLING_EXCHANGE_RATE:
      return {
        ...oldState,
        loading: false,
        error: null,
        fxRate: null
      }

    case Types.RECEIVE_EXCHANGE_RATE_SUCCESS:
      return {
        ...oldState,
        loading: false,
        error: null,
        fxRate: action.rateData.rates[action.target]
      }

    case Types.RECEIVE_EXCHANGE_RATE_ERROR:
    case Types.CREATE_NEW_EXCHANGE_TRANSACTION_ERROR:
      return {
        ...oldState,
        loading: false,
        error: true,
        fxRate: false
      }

    default:
      return oldState;
  }
}

export default exchangeReducer;
