import { combineReducers } from 'redux';
import rates from './rates';
import balances from './balances';
import exchange from './exchange';

const RootReducer = combineReducers({
  rates,
  balances,
  exchange
});

export default RootReducer;
