import Types from '../actions/types';
// This is a mock of initial state.
// This data would normally be requested from the backend server API
const _initialState = {
  'GBP' : 100,
  'EUR' : 50,
  'USD' : 34
};

const balancesReducer = (oldState = _initialState, action) => {
  switch(action.type) {
    case Types.CREATE_NEW_EXCHANGE_TRANSACTION:
      return {
        ...oldState,
        [action.baseCurrency]: oldState[action.baseCurrency] - action.baseAmount,
        [action.targetCurrency]: oldState[action.targetCurrency] + action.targetAmount,
      }

    default:
      return oldState;
  }
};

export default balancesReducer;
