export const selectWatching = (watching, currencies) => (
  Object.keys(watching).map(baseCurrency => {
    let targetCurrencies = watching[baseCurrency];

    return Object.keys(targetCurrencies).map(targetCurrency => {
      let targetRate = targetCurrencies[targetCurrency];
      let targetCurrencyLong = currencies[targetCurrency];
      return { baseCurrency, targetCurrencyLong, targetRate };
    });
  }).reduce((a, b) => [...a, ...b])
);

export const selectFXPairs = watching => {
  let fxPairs = {};

  Object.keys(watching).forEach(baseCurrency => {
    fxPairs[baseCurrency] = [];

    let targetCurrencies = watching[baseCurrency];
    Object.keys(targetCurrencies).forEach(targetCurrency => {
      fxPairs[baseCurrency].push(targetCurrency);
    });
  });
  return fxPairs;
};
