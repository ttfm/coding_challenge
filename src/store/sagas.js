import { takeLatest, call, put, all, select,race,take } from 'redux-saga/effects';
import Types from '../actions/types';
import { fetchMultipleFX, fetchLatestFX, putCurrencySubscription, postNewExchangeTransaction } from '../util/api';
import { receiveWatchingRates } from '../actions/rates';
import { receiveExchangeRate } from '../actions/exchange';

const delay = (ms) => new Promise(res => setTimeout(res, ms));

function* createNewCurrencySubscription(action) {
  // These lines are only required to provide dummy data.
  // If there was a fully functioning backend API they would be removed.
  const getWatching = state => state.rates.watching;
  let watching = yield select(getWatching);

  try {
    let newSubscriptions = yield putCurrencySubscription(action.baseCurrency, action.targetCurrency, watching);
    yield put({ type: Types.CREATE_NEW_CURRENCY_SUBSCRIPTION_SUCCESS, newSubscriptions, fxPairs: newSubscriptions});
  } catch (error) {
    yield put({ type: Types.CREATE_NEW_CURRENCY_SUBSCRIPTION_ERROR });
  }
}

function* createNewExchangeTransaction(action) {
  try {
    // This would tell the API backend that the user wishes to convert currency.
    // eslint-disable-next-line
    let transactionStatus = yield postNewExchangeTransaction(action.baseCurrency, action.baseAmount)
    // we would then yield put to put the data in the reducer.
    // this would be something like a 'transactions' slice of the reducer (not implemented as part of this challenge)
  } catch (error) {
    yield put({ type: Types.CREATE_NEW_EXCHANGE_TRANSACTION_ERROR });
  }
}

function* pollWatchingRates(action) {
  while (true) {
    try {
      let rateData = yield fetchMultipleFX(action.fxPairs);
      yield put(receiveWatchingRates(rateData));
    } catch (error) {
      yield put({ type: Types.RECEIVE_WATCHING_RATES_ERROR });
    } finally {
      // this means we poll only every 10s even on errors
      yield call(delay, 10000);
    }
  }
}

function* watchingRatesPollWatcher() {
  while (true) {
    let action = yield take(Types.START_POLLING_WATCHING_RATES);
    yield race([
      call(pollWatchingRates, action),
      take(Types.CREATE_NEW_CURRENCY_SUBSCRIPTION_SUCCESS),
      take(Types.STOP_POLLING_WATCHING_RATES)
    ]);
  }
}

function* pollExchangeRate(action) {
  while (true) {
    try {
      let rateData = yield fetchLatestFX(action.base, [action.target]);
      yield put(receiveExchangeRate(rateData, action.target));
    } catch (error) {
      yield put({ type: Types.RECEIVE_EXCHANGE_RATE_ERROR });
    } finally {
      // this means we poll only every 10s even on errors
      yield call(delay, 10000);
    }
  }
}

function* exchangeRatePollWatcher() {
  while (true) {
    let action = yield take(Types.START_POLLING_EXCHANGE_RATE);
    yield race([
      call(pollExchangeRate, action),
      take(Types.STOP_POLLING_EXCHANGE_RATE),
    ]);
  }
}


export function* rootSaga() {
  yield all([
    takeLatest(Types.CREATE_NEW_CURRENCY_SUBSCRIPTION, createNewCurrencySubscription),
    watchingRatesPollWatcher(),
    exchangeRatePollWatcher(),
    takeLatest(Types.CREATE_NEW_EXCHANGE_TRANSACTION, createNewExchangeTransaction)
  ]);
}
