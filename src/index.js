import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import store from './store/store';

document.addEventListener('DOMContentLoaded', () => {
  if (process.env.NODE_ENV === `development`) window.store = store;


  ReactDOM.render(
    <App store={store}/>,
    document.getElementById('root')
  );
});
