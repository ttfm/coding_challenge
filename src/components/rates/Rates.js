import React from 'react';
import RateItem from './RateItem';
import { connect } from 'react-redux';
import { selectWatching, selectFXPairs } from '../../selectors/rates';
import { startPollingWatchingRates, stopPollingWatchingRates } from '../../actions/rates';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import faSpinner from '@fortawesome/fontawesome-free-solid/faSpinner';
import Search from './Search';
import './Rates.css';
import { Link } from 'react-router-dom';

const mapStateToProps = ({ rates : { watching, currencies, error, loading }}) => ({
  watching: selectWatching(watching, currencies),
  fxPairs: selectFXPairs(watching),
  loading,
  error,
});

const mapDispatchToProps = dispatch => ({
  startPollingWatchingRates: fxPairs => dispatch(startPollingWatchingRates(fxPairs)),
  stopPollingWatchingRates: () => dispatch(stopPollingWatchingRates())
});

class Rates extends React.Component {
  state = {
    searchShowing: false
  }

  componentDidMount() {
    this.props.startPollingWatchingRates(this.props.fxPairs);
  }

  componentWillUnmount() {
    this.props.stopPollingWatchingRates();
  }

  toggleSearch(option) {
    return () => {
      this.setState({ searchShowing: option }, () => {
        this.props.startPollingWatchingRates(this.props.fxPairs);
      });
    }
  }

  render({watching, error, loading} = this.props) {
    if (error) {
      return(
        <div className="rates">
          <div className="rates-header">Rates</div>
          <Link to='/'><div className="rates-cancel">Cancel</div></Link>
          <div>An Error occurred</div>
        </div>
      );
    }

    // this is when loading watching pairs from the server, not for fx rates
    if (loading) {
      return(
        <div className="rates">
          <div className="rates-header">Rates</div>
          <Link to='/'><div className="rates-cancel">Cancel</div></Link>
          <FontAwesomeIcon icon={faSpinner} spin />
        </div>
      );
    }

    if (this.state.searchShowing) {
      return(<Search hideSearch={this.toggleSearch(false)}/>)
    }

    return(
      <div className="rates">
        <Link to='/'><div className="rates-cancel">Cancel</div></Link>
        <div className="rates-header">Rates</div>
        <div
          className="rates-add-new"
          onClick={this.toggleSearch(true)}
        >Add New Currency</div>
        <div className="rates-item-container">
            { watching.map(item => (
              <RateItem
              { ...item }
              key={ item.baseCurrency + item.targetCurrencyLong }
              />
            ))
          }
        </div>
      </div>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Rates);
