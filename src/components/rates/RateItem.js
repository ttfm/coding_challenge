import React from 'react';

const RateItem = ({baseCurrency, targetCurrencyLong, targetRate}) => (
    <div className="rates-item">
      <div className="rates-from">1 {baseCurrency}</div>
      <div className="rates-to">
        <div className="rates-to-amount">{targetRate ? targetRate : '-'}</div>
        <div className="rates-to-currency">{targetCurrencyLong}</div>
      </div>
    </div>
);

export default RateItem;
