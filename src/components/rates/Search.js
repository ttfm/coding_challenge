import React from 'react';
import { connect } from 'react-redux';
import './Search.css';
import { createNewCurrencySubscription } from '../../actions/rates';

const mapStateToProps = ({ rates : { currencies }}) => ({
  currencies
});

const mapDispatchToProps = dispatch => ({
  createNewCurrencySubscription: (base, target) => dispatch(createNewCurrencySubscription(base, target))
});


const CurrencyItem = ({shortCode, longCode, checked, buttonClick}) => (
  <div
    className="currency-item"
    onClick={buttonClick}
    >
    <div>{shortCode} - {longCode}</div>
    <div>{checked ? '✓' : null}</div>
  </div>
);

class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      newCurrencyBase: null,
      newCurrencyTarget: null,
      searchInput: ''
    };
  }

  handleCurrencyClick(shortCode) {
    return () => {
      if (!this.state.newCurrencyBase) {
        return this.setState({
          newCurrencyBase: shortCode,
          searchInput: ''
        });
      }

      if (this.state.newCurrencyBase === shortCode) {
        return;
      }

      if (!this.state.newCurrencyTarget) {
        this.setState({
          newCurrencyTarget: shortCode
        }, () => {
          this.props.createNewCurrencySubscription(
            this.state.newCurrencyBase,
            this.state.newCurrencyTarget
          );
          this.setState({ newCurrencyBase: null, newCurrencyTarget: null});
          this.props.hideSearch();
        });
      }
    };
  }


  render({currencies} = this.props) {
    return(
      <div className="search">

        <div className="search-cancel" onClick={this.props.hideSearch}>Cancel</div>

        <h2 className="search-header">Select currency {this.state.newCurrencyBase ? '2' : '1'}</h2>

        <form className="search-form" onSubmit={(e) => e.preventDefault()}>
          <input
            className="search-input"
            placeholder="Search"
            type="text"
            value={this.state.searchInput}
            onChange={(e) => this.setState({searchInput: e.target.value})}
          />
        </form>

        <div className="search-item-container">
          { Object.keys(currencies).map(shortCode => {
            let searchInput = this.state.searchInput;
            let longCode = currencies[shortCode];

            if (searchInput.length > 0) {
              let re = new RegExp(searchInput, 'i');
              if (!re.test(shortCode) && !re.test(longCode)) {
                return null;
              }
            }

            return(
              <CurrencyItem
                key={shortCode}
                shortCode={shortCode}
                longCode={longCode}
                checked={this.state.newCurrencyBase === shortCode}
                buttonClick={this.handleCurrencyClick(shortCode)}
              />);
            })
          }
        </div>
      </div>
    );


  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Search);
