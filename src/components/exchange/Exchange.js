import React from 'react';
import { connect } from 'react-redux';
import { startPollingExchangeRate, stopPollingExchangeRate,
  createNewExchangeTransaction } from '../../actions/exchange';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import faCaretDown from '@fortawesome/fontawesome-free-solid/faCaretDown';
import faCaretRight from '@fortawesome/fontawesome-free-solid/faCaretRight';
import faSpinner from '@fortawesome/fontawesome-free-solid/faSpinner';
import './Exchange.css';
import { Link } from 'react-router-dom';

const mapStateToProps = ({ balances, exchange: {loading, error, fxRate} }) => ({
  currencies: Object.keys(balances).sort((a, b) => a.localeCompare(b)),
  balances,
  loading,
  error,
  fxRate,
});

const mapDispatchToProps = dispatch => ({
  startPollingExchangeRate: (base, target) => dispatch(startPollingExchangeRate(base, target)),
  stopPollingExchangeRate: () => dispatch(stopPollingExchangeRate()),
  transact: (baseCurrency, baseAmount, targetCurrency, targetAmount) => dispatch(createNewExchangeTransaction(baseCurrency, baseAmount, targetCurrency, targetAmount))
});


class Exchange extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      baseValue: '',
      targetValue: '',
      baseCurrency: 'GBP',
      targetCurrency: 'EUR',
    };
    this.baseCurrencyIterator = this.currencyIterator(this.state.baseCurrency);
    this.targetCurrencyIterator = this.currencyIterator(this.state.targetCurrency);
  }

  startPolling() {
    this.props.startPollingExchangeRate(this.state.baseCurrency, this.state.targetCurrency);
  }

  currencyIterator(currentCurrency, {currencies} = this.props) {
    let currentIndex = currencies.indexOf(currentCurrency) + 1;
    return {
      next: () => {
        if (currentIndex < currencies.length) {
          return {value: currencies[currentIndex++], done: false};
        } else {
          currentIndex = 0;
          return {value: currencies[currentIndex++], done: false};
        }
      }
    };
  }

  currencyChange(e, type) {
    e.preventDefault();
    this.props.stopPollingExchangeRate();
    if (type === 'base') {
      this.setState({
        baseCurrency: this.baseCurrencyIterator.next().value,
      }, this.startPolling);
    }
    if (type === 'target') {
      this.setState({
        targetCurrency: this.targetCurrencyIterator.next().value,
      }, this.startPolling);
    }
  }

  displayInputValue(type) {
    if (type === 'base') {
      return this.state.baseValue ? '-' + this.state.baseValue : '';
    }
    if (type === 'target') {
      return this.state.targetValue ? '+' + this.state.targetValue : '';
    }
  }

  componentDidMount() {
    this.startPolling();
    this.baseDiv.focus();
  }

  componentWillUnmount() {
    this.props.stopPollingExchangeRate();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.fxRate !== this.props.fxRate) {
      this.setExchangeValues();
    }
  }

  setExchangeValues(baseValue, targetValue, { fxRate } = this.props) {
    const formatNumber = (number) => {
      let v = number.toString();
      // replace plus and minus
      v = v.replace(/[+-]/g, '');
      // remove non digits
      v = v.replace(/[^0-9.]/g, '');
      // remove extraneous decimals
      v = v.replace(/(\.\d*)\./g, '$1');
      // limit to two decimal places
      v = v.replace(/(\.\d{0,2})\d*/g, '$1');
      return v;
    };

    if (baseValue === undefined && targetValue === undefined) {
      return this.setState({
        baseValue: formatNumber(this.state.baseValue) || '',
        targetValue: formatNumber(fxRate * parseFloat(this.state.baseValue)) || ''
      });
    }

    if (targetValue === undefined) {
      return this.setState({
        baseValue: formatNumber(baseValue) || '',
        targetValue: formatNumber(fxRate * parseFloat(formatNumber(baseValue))) || ''
      });
    }

    if (baseValue === undefined) {
      return this.setState({
        baseValue: formatNumber((1 / fxRate) * parseFloat(formatNumber(targetValue))) || '',
        targetValue: formatNumber(targetValue) || ''
      });
    }
  }


  handleInput(e, box) {
    let rawText = e.target.value;

    if (box === 'top') {
      return this.setExchangeValues(rawText);
    }

    if (box === 'bottom') {
      return this.setExchangeValues(undefined, rawText);
    }
  }

  exchangeButton({
    baseValue,
    targetValue,
    baseCurrency,
    targetCurrency} = this.state
  ) {
    const isEnabled = baseValue &&
                       (baseValue) > 0 &&
                       this.props.fxRate &&
                       baseCurrency !== targetCurrency &&
                       this.props.balances[baseCurrency] >= baseValue;

    return(
      <button
        className='exchange-button'
        disabled={!isEnabled}
        onClick={() => this.props.transact(baseCurrency,
                                           parseFloat(baseValue),
                                           targetCurrency,
                                           parseFloat(targetValue))
                }
      >EXCHANGE
      </button>
    );
  }

  render({baseCurrency, targetCurrency} = this.state) {
    const currencyRate = (<div>
      1 {baseCurrency} = {
        this.props.fxRate ?
        <span>
         {this.props.fxRate.toFixed(4).slice(0, -2)}
         <span>{this.props.fxRate.toFixed(4).slice(-2)}</span>
        </span>
        :
        ' '
      } {targetCurrency}
    </div>);

    return(
      <div className='exchange-box'>
        <div className='exchange-dropdown'>

        { this.props.loading || !this.props.fxRate ?
          <FontAwesomeIcon icon={faSpinner} spin /> :
          currencyRate
        }

          <Link to='/rates'>
            <FontAwesomeIcon icon={faCaretDown} pull='right' />
          </Link>

        </div>

        <div className='exchange-top'>
          <div className="exchange-left">

            <div className="exchange-currency">
              {baseCurrency}
              <FontAwesomeIcon
                className='exchange-change-currency'
                icon={faCaretRight}
                pull='right'
                size="lg"
                onClick={(e) => this.currencyChange(e, 'base')}
              />
            </div>

            <div className="exchange-balance">
            You have {this.props.balances[baseCurrency].toFixed(2) || 0}
            </div>
          </div>

          <div
            className="exchange-right"
            onClick={() => {
              this.baseDiv.focus();
            }}
          >
            <input
              ref={div => this.baseDiv = div}
              className="exchange-input"
              value={this.displayInputValue('base')}
              onChange={(e) => this.handleInput(e, 'top')}
            />
          </div>
        </div>

        <div className='exchange-bottom'>
          <div className="exchange-left">

            <div className="exchange-currency">
              {targetCurrency}
              <FontAwesomeIcon
                className='exchange-change-currency'
                icon={faCaretRight}
                pull='right'
                size="lg"
                onClick={(e) => this.currencyChange(e, 'target')}
              />
            </div>

            <div className="exchange-balance">
            You have {this.props.balances[targetCurrency].toFixed(2) || 0}
            </div>
          </div>

          <div
            className="exchange-right"
            onClick={() => {
              this.targetDiv.focus();
            }}
          >
            <input
              ref={div => this.targetDiv = div}
              className="exchange-input"
              value={this.displayInputValue('target')}
              onChange={(e) => this.handleInput(e, 'bottom')}
            />
          </div>
        </div>
        { this.exchangeButton() }
      </div>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Exchange);
