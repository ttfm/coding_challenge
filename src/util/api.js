import axios from 'axios';

const API_URL = "https://data.fixer.io/api/";
const API_MAX_WAIT_TIME = 5000;

export const fetchLatestFX = (base, targets) => (
  axios.get(`${API_URL}latest`, {
      params: {
        access_key: 'c8021666e80d5d8f867542898970eb1b',
        symbols: targets.join(),
        base
      },
      timeout: API_MAX_WAIT_TIME
    })
    .then(function (response) {
      if (response.data.success) {
        return response.data
      } else {
        throw response.data.error;
      }
    })
    .catch(function (error) {
      throw error;
    })
);

// fxPairs format is:
// {'EUR' : ['GBP','USD']}
export const fetchMultipleFX = async fxPairs => {
  let fns = []

  Object.keys(fxPairs).forEach(base => {
    fns.push(fetchLatestFX(base, fxPairs[base]))
  });

  const fxData = await Promise.all(fns)
  let fxRates = {};

  fxData.forEach(data => {
    fxRates[data.base] = data.rates;
  });

  return fxRates;
};


// Dummy method to tell backend API user wishes to convert currency.
export const postNewExchangeTransaction = (baseCurrency, baseAmount) => {
  // This would call the backend API using something like axios.put()
}

// Dummy method to tell the backend API we want to add a new currency pair to watch for this user.
// The final parameter is not needed with a working backend.
export const putCurrencySubscription = (baseCurrency, targetCurrency, watching) => {
  try {
    // this would normally be a remote API call to the backend e.g. axios.put() which returns JSON data upon success.
    // dummy data as no API
    let data = {
      ...watching,
      [baseCurrency] : {
        ...watching[baseCurrency],
        [targetCurrency] : null
      }
    }
    return data
  } catch (error) {
    throw error
  }
};
