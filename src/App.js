import React, { Component } from 'react';
import './App.css';
import { Provider } from 'react-redux';
import Rates from './components/rates';
import Exchange from './components/exchange';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from 'react-router-dom';

import './util/api';

class App extends Component {
  render() {
    return (
      <Provider store={this.props.store}>
        <Router>
          <div className="app">

            <Switch>
                <Route path="/" exact={true} component={Exchange} />
                <Route path="/rates" exact={true} component={Rates} />
                <Redirect to="/" />
            </Switch>


          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
