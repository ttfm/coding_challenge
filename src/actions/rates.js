import Types from './types';

export const startPollingWatchingRates = fxPairs => ({
  type: Types.START_POLLING_WATCHING_RATES,
  fxPairs
});

export const stopPollingWatchingRates = () => ({
  type: Types.STOP_POLLING_WATCHING_RATES
});

export const receiveWatchingRates = rateData => ({
  type: Types.RECEIVE_WATCHING_RATES_SUCCESS,
  rateData
});

export const createNewCurrencySubscription = (baseCurrency, targetCurrency) => ({
  type: Types.CREATE_NEW_CURRENCY_SUBSCRIPTION,
  baseCurrency,
  targetCurrency
});
