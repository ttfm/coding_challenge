import Types from './types';

export const startPollingExchangeRate = (base, target) => ({
  type: Types.START_POLLING_EXCHANGE_RATE,
  base,
  target
});

export const stopPollingExchangeRate = () => ({
  type: Types.STOP_POLLING_EXCHANGE_RATE
});

export const receiveExchangeRate = (rateData, target) => ({
  type: Types.RECEIVE_EXCHANGE_RATE_SUCCESS,
  rateData,
  target
});

export const createNewExchangeTransaction = (
  baseCurrency,
  baseAmount,
  targetCurrency,
  targetAmount
) => ({
  type: Types.CREATE_NEW_EXCHANGE_TRANSACTION,
  baseCurrency,
  baseAmount,
  targetCurrency,
  targetAmount
});
